#!/bin/bash

sum=0
declare -a delete_list

while read size path; do
    if [ $size -lt 5 ]; then
        delete_list+=("$path")
        sum=$((sum + size))
    fi
done < <(du -h -d 1 -m)

echo "Total size to be deleted: $sum MB"
echo "Files to be deleted: ${delete_list[@]}"

read -p "Are you sure you want to delete these files? (y/n) " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    for file in "${delete_list[@]}"; do
        rm -rf "$file"
    done
fi